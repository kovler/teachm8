# from django.contrib.auth.models import User, Group
from models import Faculty, College, Course, Request, Teacher, Student
from rest_framework import serializers


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course


class FacultySerializer(serializers.ModelSerializer):
    course_set = CourseSerializer(many=True, read_only=True)

    class Meta:
        model = Faculty


class CollegeSerializer(serializers.ModelSerializer):
    faculty_set = FacultySerializer(many=True, read_only=True)

    class Meta:
        model = College
        fields = ('id','name', 'faculty_set')

class RequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Request

class TeacherSerializer(serializers.ModelSerializer):
    request_set = RequestSerializer(many=True, read_only=True)

    class Meta:
        model = Teacher

class StudentSerializer(serializers.ModelSerializer):
    # request_set = RequestSerializer(many=True, read_only=True)
    college = CollegeSerializer()

    class Meta:
        model = Student
        fields = ('id','college')





