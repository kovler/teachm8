from __future__ import unicode_literals

from django.apps import AppConfig


class Teachm8AppConfig(AppConfig):
    name = 'teachm8app'
