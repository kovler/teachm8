from django.conf.urls import url

from . import views

from django.conf.urls import url, include, patterns
from rest_framework import routers
import views
from django.conf import settings


router = routers.DefaultRouter()
router.register(r'colleges', views.CollegeViewSet)
router.register(r'student', views.StudentViewSet)
# router.register(r'dashboard_teacher', views.TeacherDashboardViewSet)
# router.register(r'requests', views.RequestView)



# app_name = 'teachm8'
urlpatterns = [
    url(r'^accounts/', include('registration.backends.hmac.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^$', views.HomeView.as_view(), name='index'),
    url(r'^', include(router.urls)),
    url(r'^how_it_works/', views.HowItWorksView.as_view(), name='how_it_works'),
    url(r'^about/', views.AboutView.as_view(), name='about'),
    url(r'^teachers/(?P<course_id>[0-9]+)/', views.TeachersView.as_view(), name='teachers'),
    url(r'^dashboard/', views.DashboardView.as_view(), name='dashboard'),
    url(r'^message_teacher', views.RequestView.as_view(), name="request_view"),
    url(r'^update-profile/(?P<option>[a-z]+)', views.UpdateProfileView.as_view(), name="update-profile"),
    url(r'^teacher-profile/(?P<teacher_id>[0-9]+)', views.TeacherProfileView.as_view(), name="teacher-profile"),
    url(r'^contact-form', views.ContactFormView.as_view(), name="contact-form"),
]


if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))