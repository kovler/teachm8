from django.views.generic import View
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from .models import User, Teachm8user, Teacher, Student, College, Faculty, Course, Request, Review
from rest_framework import viewsets
from django.http import HttpResponse, HttpResponseRedirect
from serializers import CollegeSerializer, TeacherSerializer, StudentSerializer, RequestSerializer
from rest_framework import filters

class HomeView(View):
    def get(self, request):
        return render(request, 'teachm8app/home.html', {})


class HowItWorksView(View):
    def get(self, request):
        return render(request, 'teachm8app/how_it_works.html', {})


class AboutView(View):
    def get(self, request):
        return render(request, 'teachm8app/about.html', {})


class UpdateProfileView(View):
    def get(self, request, option):
        title = ''
        if option == 'info':
            title = 'Profile updated'
            context = {'title': title, 'include_form': False}
            return render(request, 'teachm8app/update-profile.html', context)
        else:
            form_fields = [{'title': 'Name', 'name': 'first_name', 'type': 'text'},
                           {'title': 'Last name', 'name': 'last_name', 'type': 'text'},
                           # {'title': 'Picture', 'name': 'picture', 'type': 'file'},
                           # {'title': 'Location', 'name': 'location', 'type': 'text'},
                           {'title': 'Phone number', 'name': 'phone', 'type': 'tel'}]
            if option == 'editprofile':
                title = 'Edit your profile'
            elif option == 'completeprofile':
                title = 'It seems that your profile needs to be completed before moving on...'

            context = {'title': title, 'form_fields': form_fields, 'include_form': True}
            return render(request, 'teachm8app/update-profile.html', context)

    def post(self, request, option):
        if option == 'info':
            user = request.user
            user.first_name = request.POST["first_name"]
            user.last_name = request.POST["last_name"]
            user.save()
            # user.teachm8user.img = request.POST["picture"]
            college_id = request.POST["college"]
            user.teacher.college = College.objects.filter(id=college_id)[0]
            user.teacher.save()
            user.student.college = College.objects.filter(id=college_id)[0]
            user.student.save()
            user.teachm8user.location = request.POST["location"]
            user.teachm8user.phone = request.POST["phone"]
            user.teachm8user.save()

            title = 'Profile updated'
            context = {'title': title, 'include_form': False}
            return render(request, 'teachm8app/update-profile.html', context)


class TeachersView(View):
    def get(self, request, course_id):
        course = Course.objects.get(pk=course_id)
        context = {}
        # If a user has logged in
        if not request.user.is_anonymous():
            # If the user has registered for this course as a teacher
            try:
                if request.user.teacher.course.filter(id=course_id):
                    context = {"message": "You have already signed up for this course as a teacher", "course_name": course.name}
                    return render(request, "teachm8app/teachers.html", context)
            except:
                pass

        teachers_list = []
        teachers = Teacher.objects.filter(course=course)
        if teachers:
            teacher_avgs = []
            for teacher in teachers:
                teacher_avg = get_avg(teacher)
                teachers_list.append({'teacher': teacher, 'avg': teacher_avg})
            context = {"course": course_id, "course_name": course.name, "teachers": teachers_list}
        else:
            context = {"course_name": course.name, "message": "No teachers available"}
        return render(request, "teachm8app/teachers.html", context)


class DashboardView(View):
    def get(self, request):
        user = request.user

        # If the user has not finished his profile point to the updating profile page
        if not (user.first_name and user.last_name):
            # return render(request, "teachm8app/update-profile.html", context)
            return redirect('update-profile', option='completeprofile')

        # ratings = Review.objects.filter(teacher=user.teacher)
        # sum = 0
        #
        # for rating in ratings:
        #     sum += rating.rate
        # if len(ratings) == 0:
        #     avg = 0
        # else:
        #     avg = sum/len(ratings)

        context = {}

        ####### Teacher tab ########
        try:
            # Students who have requested this user as a teacher
            requests_from_students = Request.objects.filter(teacher=user.teacher)

            # Reviews for this user as a teacher
            reviews = Review.objects.filter(teacher=user.teacher)

            # List of courses of this teacher
            courses_as_teacher = Course.objects.filter(teacher=user.teacher)

            # Rating
            avg_rate = get_avg(user.teacher)

            context['avg_rate'] = avg_rate
            context['requests_from_students'] = requests_from_students
            context['reviews'] = reviews
            context['courses_as_teacher'] = courses_as_teacher
        except:
            pass

        ####### Student tab ########
        try:
            # Teachers who this student has requested
            requests_to_teachers = Request.objects.filter(student=user.student)

            # List of courses of this user as student
            courses_as_student = Course.objects.filter(student=user.student)

            context['requests_to_teachers'] = requests_to_teachers
            context['courses_as_student'] = courses_as_student
        except:
            pass

        # context = {"requests_from_students": requests_from_students, "reviews": reviews, "courses_as_teacher": courses_as_teacher,
        #            "requests_to_teachers": requests_to_teachers, "courses_as_student": courses_as_student}

        return render(request, "teachm8app/dashboard.html", context)

def get_avg(teacher):
    reviews = Review.objects.filter(teacher=teacher)
    reviews_sum = 0
    if len(reviews) == 0:
        return 'No ratings yet'
    for review in reviews:
        reviews_sum += review.rate
    return round(float(reviews_sum)/len(reviews), 2)


# def post_message_to_teacher(teacher, student, course, message, date, time):
class RequestView(View):
    def post(self, request):
        comment = request.POST["comment"]
        course_id = int(request.POST["courseId"])
        course = Course.objects.filter(pk=course_id)[0]
        teacher_id = request.POST["teacherId"]
        teacher = Teacher.objects.filter(pk=teacher_id)[0]
        # Check
        try:
            request.user.student
        except:
            new_student = Student(user=request.user)
            new_student.save()
        student = request.user.student
        if course not in student.course.all():
            student.course.add(course)
            student.faculty = course.faculty
            student.college = course.faculty.college
        student.save()
        req = Request(teacher=teacher, student=student, course=course, comment=comment)
        req.save()
        return HttpResponse(status=200)


class TeacherProfileView(View):
    def get(self, request, teacher_id):
        teacher = Teacher.objects.filter(user_id=teacher_id)[0]
        context = {'teacher': teacher}
        return render(request, "teachm8app/teacher-profile.html", context)


class ContactFormView(View):
    def get(self, request):
        context = {}
        return render(request, "teachm8app/contact-form.html", context)


class CollegeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # queryset = College.objects.all()
    queryset = College.objects.all()
    serializer_class = CollegeSerializer
    # filter_backends = (filters.DjangoFilterBackend,)
    # filter_fields = ('id',)


class StudentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Student.objects.all()
    def get_queryset(self):
        if self.request.user.is_anonymous():
            return []
        else:
            return Student.objects.filter(user=self.request.user)

    serializer_class = StudentSerializer
    # filter_backends = (filters.DjangoFilterBackend,)
    # filter_fields = ('id',)


class TeacherDashboardViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer






