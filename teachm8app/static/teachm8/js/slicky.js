///**
// * Created by User on 23/02/2016.
// */
//$(document).ready(function() {
//  $('.teacher-container').slick({
//    dots: true,
//    infinite: false,
//    speed: 300,
//    slidesToShow: 4,
//    slidesToScroll: 4,
//    responsive: [
//      {
//        breakpoint: 1024,
//        settings: {
//          slidesToShow: 3,
//          slidesToScroll: 3,
//          infinite: true,
//          dots: true
//        }
//      },
//      {
//        breakpoint: 600,
//        settings: {
//          slidesToShow: 2,
//          slidesToScroll: 2
//        }
//      },
//      {
//        breakpoint: 480,
//        settings: {
//          slidesToShow: 1,
//          slidesToScroll: 1
//        }
//      }
//      // You can unslick at a given breakpoint now by adding:
//      // settings: "unslick"
//      // instead of a settings object
//    ]
//  })
//});

$(document).ready(function () {

// SLick Slider Code
    var $windowWidth = $(window).width();

    $(window).resize(function () {
        var $windowWidth = $(window).width();
        if (($windowWidth > 1) && ($windowWidth < 640)) {
            $('.slick-multislider').unslick();
            $('.slick-multislider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                //autoplaySpeed: 2000,
                dots: true
            });

        } else {
            $('.slick-multislider').unslick();
            $('.slick-multislider').slick({
                slidesToShow: 3,
                slidesToScroll: 2,
                autoplay: false,
                //autoplaySpeed: 2000,
                dots: true
            });
        }

    });

    if (($windowWidth > 1) && ($windowWidth < 640)) {

        $('.slick-multislider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            autoplay: false,
            //autoplaySpeed: 2000,
            dots: true
        });

    } else {
        $('.slick-multislider').slick({
            slidesToShow: 3,
            slidesToScroll: 2,
            infinite: true,
            autoplay: false,
            //autoplaySpeed: 2000,
            dots: true
        });
    }
});