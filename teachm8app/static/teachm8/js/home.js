// Code using $ as usual goes here.
var colleges_json;
var step = 0;
var major_name = "";
var course_names = [];
var courses;
var faculties;
var teacherId = -1;


// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

$(document).ready(function () {
    $.get("http://127.0.0.1:8000/teachm8/student/", function(data){
        //if the list is empty it means that no user is logged
        var collegeNames;
        if (data.length == 0) {
            //Make a request for all colleges
            $.get("http://127.0.0.1:8000/teachm8/colleges/").done(function (colleges) {
                display_home();
                var collegeNames = colleges.map(function (i) {
                    return i.name
                });

                $("#input-university").autocomplete({
                    source: collegeNames
                });
                colleges_json = colleges;
            });
        }
        //else: a user is currently logged in.
        else {
            display_home();
            var collegeName = data.map(function (student) {
                return student.college.name
            });

            $("#input-university").autocomplete({
                source: collegeName
            });
            colleges_json = [data[0].college];
            populate();
        }
    });
});

$("#btn-findTeacher").click(function () {
    display_q1();
    $("#second-stripe").hide();
});

$("#btn-university").click(function () {
    //open second step
    display_q2();
    var college_name = $("#input-university").val();
    var college = colleges_json.filter(function (i) {
        return i.name === college_name
    })[0];
    faculties = college.faculty_set;
    var facultiesNames = faculties.map(function (i) {
        return i.name
    });

    $("#input-major").autocomplete({
        source: facultiesNames
    });
});

$("#btn-major").click(function () {
    //open third step
    display_q3();
    major_name = $("#input-major").val();
    var major = faculties.filter(function (i) {
        return i.name === major_name
    })[0];
    courses = major.course_set;
    var courseNames = courses.map(function (i) {
        return i.name
    });

    $("#input-course").autocomplete({
        source: courseNames
    });
});

$("#btn-course").click(function () {
    //getting id of the course
    course_name = $("#input-course").val();
    var course = courses.filter(function (i) {
        return i.name === course_name
    })[0];
    var courseId = course.id;
    window.location.href = "http://127.0.0.1:8000/teachm8/teachers/" + courseId;
});

function display_home() {
    $("#home").show();
    $("#home-q1").hide();
    $("#home-q2").hide();
    $("#home-q3").hide();
    $("#bar-step1").hide();
    $("#bar-step2").hide();
    $("#bar-step3").hide();
}

function display_q1() {
    $("#home").hide();
    $("#home-q1").show();
    $("#home-q2").hide();
    $("#home-q3").hide();
    $("#bar-step1").show();
    $("#bar-step2").hide();
    $("#bar-step3").hide();
}

function display_q2() {
    $("#home").hide();
    $("#home-q1").hide();
    $("#home-q2").show();
    $("#home-q3").hide();
    $("#bar-step1").hide();
    $("#bar-step2").show();
    $("#bar-step3").hide();
}

function display_q3() {
    $("#home").hide();
    $("#home-q1").hide();
    $("#home-q2").hide();
    $("#home-q3").show();
    $("#bar-step1").hide();
    $("#bar-step2").hide();
    $("#bar-step3").show();
}

/************* TEACHERS **************/
$(".btn-message-teacher-not-authenticated").click(function () {
    window.location.href = "http://127.0.0.1:8000/teachm8/accounts/register/";
    //{% url 'registration_register' %}
});

/************* DASHBOARD **************/
$(function () {
    $(".dashboard .student-info").hide();
});

$("#teacher-btn").click(function () {
    $(".dashboard .student-info").hide();
    $(".dashboard .teacher-info").show();
});

$("#student-btn").click(function () {
    $(".dashboard .teacher-info").hide();
    $(".dashboard .student-info").show();
});

$("#btn-findTeacher-2").click(function () {
    window.location.href = "http://127.0.0.1:8000/teachm8/";
    //{% url 'registration_register' %}
    //TODO: display not working
    $(function () {
        display_q1();
    });
});


$(".btn-message").click(function (event) {
    teacherId = event.target.value;
});

//Send message
$("#btn-send-message-to-teacher").click(function () {
    $.post("http://127.0.0.1:8000/teachm8/message_teacher", {
            comment: $("#message-teacher-text").val(),
            courseId: courseId,
            teacherId: teacherId
        })
        .done(function (data) {
            $("#close-message-to-teacher").trigger("click");
     });
});

/************* UPDATE/EDIT PROFILE **************/
$("#save_profile").click(function () {
    $.post("http://127.0.0.1:8000/teachm8/update-profile/info", {
            first_name: $("#first_name")[0].value,
            last_name: $("#last_name")[0].value,
            //picture: $("#picture")[0].value,
            college: $(".select-menu option:selected").val(),
            location: $("#pac-input")[0].value,
            phone: $("#phone")[0].value
        })
        .done(function () {
            console.log('Success');
            window.location.href = "http://127.0.0.1:8000/teachm8/update-profile/info";
     });
});

function addOptionToSelectList(selector, value, name){
  $(selector)
      .append("<option value="+value+">"+name+"</option>");
}

function populate (){
    $.get("http://127.0.0.1:8000/teachm8/colleges/").done(function (colleges) {
        colleges.forEach(function(college){
            addOptionToSelectList('#sel1', college.id, college.name);
        });
    });
}
//populate(colleges_json);

/************* REGISTRATION **************/


/**************** OTHER ******************/

