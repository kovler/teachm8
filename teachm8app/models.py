from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


@python_2_unicode_compatible
class Loc(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class College(models.Model):
    name = models.CharField(max_length=200)
    location = models.ForeignKey(to=Loc)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Faculty(models.Model):
    name = models.CharField(max_length=200)
    college = models.ForeignKey(to=College)

    def __str__(self):
        return self.name + ' (' + str(self.college.name) + ')'


@python_2_unicode_compatible
class Course(models.Model):
    name = models.CharField(max_length=200)
    faculty = models.ForeignKey(to=Faculty)

    def __str__(self):
        return self.name + ' (' + str(self.faculty.college.name) + ') '


@python_2_unicode_compatible
class Teachm8user(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    img = models.ImageField(blank=True, null=True)
    phone = models.IntegerField(default=0, blank=True, null=True)
    location = models.CharField(max_length=200, default='-')
    # location = models.ForeignKey(to=Loc, blank=True, default=0)

    def __str__(self):
        return self.user.username


@python_2_unicode_compatible
class Teacher(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    college = models.ForeignKey(to=College, blank=True, null=True)
    faculty = models.ForeignKey(to=Faculty, blank=True, null=True)
    course = models.ManyToManyField(to=Course, blank=True, null=True)
    description = models.CharField(default='No description yet', max_length=254)

    def __str__(self):
        return self.user.username


@python_2_unicode_compatible
class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    college = models.ForeignKey(to=College, blank=True, null=True)
    faculty = models.ForeignKey(to=Faculty, blank=True, null=True)
    course = models.ManyToManyField(to=Course, blank=True, null=True)

    def __str__(self):
        return self.user.username


@python_2_unicode_compatible
class Review(models.Model):
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    rate = models.IntegerField(default=0)
    text = models.CharField(default='', max_length=200)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    date = models.DateField

    def __str__(self):
        return str(self.rate)


@python_2_unicode_compatible
class Request(models.Model):
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    comment = models.CharField(max_length=200)
    date_and_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'Request from ' + str(self.student) + ' to study ' + str(self.course) + ' with ' + str(self.teacher) + ' | ' + str(self.date_and_time)


@receiver(post_save, sender=User)
def my_handler(sender, **kwargs):
    if kwargs["created"]:
        new_teachm8user = Teachm8user(user=kwargs["instance"])
        new_teachm8user.save()
        new_teacher = Teacher(user=kwargs["instance"])
        new_teacher.save()
        new_student = Student(user=kwargs["instance"])
        new_student.save()
