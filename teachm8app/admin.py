from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from .models import College, Faculty, Course, Review, Loc, Request, Teacher, Student, Teachm8user

class TeacherInline(admin.StackedInline):
    model = Teacher
    can_delete = False
    verbose_name_plural = 'teacher'

class StudentInline(admin.StackedInline):
    model = Student
    can_delete = False
    verbose_name_plural = 'student'

class Teachm8userInline(admin.StackedInline):
    model = Teachm8user
    can_delete = False
    verbose_name_plural = 'teachm8user'

class UserAdmin(BaseUserAdmin):
    inlines = (Teachm8userInline, TeacherInline, StudentInline)

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Teacher)
admin.site.register(Student)
admin.site.register(Teachm8user)
admin.site.register(College)
admin.site.register(Faculty)
admin.site.register(Course)
admin.site.register(Review)
admin.site.register(Loc)
admin.site.register(Request)
